---
title: About
layout: single
permalink: /about/
author_profile: true
---

Outside of my field, I enjoy time with my family—including my wife and dog—who keep me very busy and help to keep me sane. I also have a fascination with a wide range of sciences that I wish I had the time to devote to researching.

Much of this site is devoted to my thoughts and ramblings on various matters and so will contain material that is subject to strong bias; you are encouraged to construct your own opinions. Formal papers contain no such influence without rationale and references.

I changed GPG keys in July 2019; see my [key transition statement](https://www.chadporter.net/key-transition.txt), signed with my [new](https://www.chadporter.net/key-transition.txt.new.asc) key.