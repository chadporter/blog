---
title: "Bastion"
layout: secret_post
classes: wide
excerpt: "This post is a write-up for the Bastion box on [hackthebox.eu](https://www.hackthebox.eu)"
date: 2019-09-07T14:30:30-04:00
key: "3oTuVEflH3842cld71iTI_hUNA3APoMi6epiBUk8ZoThun0q3sUxl+UboXiCAsWU"
tags:
  - hackthebox
  - bastion
  - htb
  - writeups
  - mRemoteNG
  - smb
  - vhd
  - hash
  - bruteforce
---

This post is a write-up for the Bastion box on hackthebox.eu
<figure>
    <a href="https://blog.chadp.me/assets/images/bastion/BLieUh18xIKESzWo.png"><img src="https://blog.chadp.me/assets/images/bastion/BLieUh18xIKESzWo.png"></a>
  <figcaption>Bastion Info</figcaption>
</figure>

### Enumeration

Start by enumerating the ports on the victim machine. Run `Masscan` and `Nmap`, then document the results:
```
masscan -e tun0 -p1-65535,U:1-65535 10.10.10.134 --max-rate=500
nmap -n -v -Pn -p80 -A --reason -oN bastion_nmap.txt 10.10.10.134
```
<figure>
    <a href="https://blog.chadp.me/assets/images/bastion/ey0zmGeHRMlfspOS.png"><img src="https://blog.chadp.me/assets/images/bastion/ey0zmGeHRMlfspOS.png"></a>
  <figcaption>Running nmap reveals 4 open ports</figcaption>
</figure>

Checked port 445 with `smbmap`, and noticed a readable and writable share called Backups:
<figure>
    <a href="https://blog.chadp.me/assets/images/bastion/Pe1qC32VoVzXAglG.png"><img src="https://blog.chadp.me/assets/images/bastion/Pe1qC32VoVzXAglG.png"></a>
  <figcaption>SMBMap shares</figcaption>
</figure>

Given that this is a Windows victim, at this point I usually switch over to a Windows VM. I have been using Commando VM 2.0 lately, and I cannot recommend it enough especially for the task of mounting SMB shares.

### User flag
Connect to the share via windows explorer and notice it has 2 .vhd backup files in it. The .vhd backups are quite large, so to make this faster you can simply mount them over the smb share:
<figure>
    <a href="https://blog.chadp.me/assets/images/bastion/UUH6FqB7OD5r2Y11.png"><img src="https://blog.chadp.me/assets/images/bastion/UUH6FqB7OD5r2Y11.png"></a>
  <figcaption>vhd mount</figcaption>
</figure>

You should be able to read the backup of the c drive from `L4mpje`. Digging through the backup did not result in anything interesting, but you can pull the `SAM` and `SYSTEM` file from `C:\windows\system32\config` to the attacking machine for some hash cracking fun:
```
pwdump SYSTEM SAM
Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
L4mpje:1000:aad3b435b51404eeaad3b435b51404ee:26112010952d963c8dc4217daec986d9:::
```

Use hashkiller to bruteforce the hashed password of user `L4mpje`
```
L4mpje:bureaulampje
```

SSH into the victim machine with the credentials to get the user flag:
<figure>
    <a href="https://blog.chadp.me/assets/images/bastion/Mk4dXNfshmTEcLAm.png"><img src="https://blog.chadp.me/assets/images/bastion/Mk4dXNfshmTEcLAm.png"></a>
  <figcaption>user flag</figcaption>
</figure>

### Root Flag
Enumerate the victim machine, and locate an application called `mRemoteNG`.

`mRemoteNG` manages connections (to ssh, rdp, ftp, etc.) and saves the connection details in an xml, including the encrypted saved credentials. It saves these encrypted secrets in `C:\users\l4mpje\AppData\Roaming\mRemoteNG\confCons.xml`. We could attempt to bruteforce these, but that might take a long time.

Easier method: Install `mRemoteNG` on the attackimg machine and copy in the `confCons.xml` file.

Connect as administrator to the victim machine (change the IP & Protocol accordingly).
<figure>
    <a href="https://blog.chadp.me/assets/images/bastion/KBTK23je0i9xWY91.png"><img src="https://blog.chadp.me/assets/images/bastion/KBTK23je0i9xWY91.png"></a>
  <figcaption>root flag</figcaption>
