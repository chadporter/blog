---
title: "Luke"
classes: wide
excerpt: "This post is a write-up for the Luke box on [hackthebox.eu](https://www.hackthebox.eu)"
date: 2019-09-14T14:30:30-04:00
tags:
  - hackthebox
  - luke
  - htb
  - writeups
  - Python
  - Ajenti
  - enumeration
---
This post is a write-up for the Luke box on hackthebox.eu
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/tX3DUnQooefWpzBN.png"><img src="https://blog.chadp.me/assets/images/luke/tX3DUnQooefWpzBN.png"></a>
  <figcaption>Luke Info</figcaption>
</figure>

### Enumeration

Start enumerating the ports on the victim machine by running `Nmap` and `Masscan`:
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/lbWfPlFFkCGKpYUP.png"><img src="https://blog.chadp.me/assets/images/chainsaw/lbWfPlFFkCGKpYUP.png"></a>
  <figcaption>Running nmap reveals three open ports</figcaption>
</figure>

Initial recon results in the following information:
- Port 21
   - FTP server which allows anonymous login
- Port 22
   - SSH server
- Port 80
   - HTTP server running PHP (Apache/2.4.38 (FreeBSD) PHP/7.3.3)
-  Port 3000
   - Node.js Express server likely running a JSON Rest API (application/json; charset=utf-8)
- Port 8000
   - HTTP Control Panel called `Ajenti`

Check out the FTP Server:
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/BvcExm8Hkog4IvGN.png"><img src="https://blog.chadp.me/assets/images/luke/BvcExm8Hkog4IvGN.png"></a>
  <figcaption>Getting the file on the FTP server</figcaption>
</figure>

From the FTP server, locate a file called `for_Chihiro.txt`.
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/OWs1K73s0OdkPiFN
.png"><img src="https://blog.chadp.me/assets/images/luke/OWs1K73s0OdkPiFN
.png"></a>
  <figcaption>The contents of the file from the FTP server</figcaption>
</figure>

Thinking about the file, I made a few assumptions:
- Obviously, a Junior Web Developmer - Chihiro
- Someone who is advising him - `Derry`
- The web page is likely using a standard framework; `Derry` says that `Chihiro` should "know where to look".

Browsing to the victim machine on port 80 results in a Single Page App (SPA) with a standard template. The page indicates that it uses `Bootstrap 4`. Look at the source of the page and notice that the third party sources are in the `vendor` directory, while the standard are in `css` and `js`. This is pretty standard practice in web development. Open the source of `css`, and notice a reference to `Blackrock Digital’s Start Bootstrap Theme Framework`.
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/1wUGaiCYYaJoqTQY.png"><img src="https://blog.chadp.me/assets/images/luke/1wUGaiCYYaJoqTQY.png"></a>
  <figcaption>The contents of the CSS file with reference to Start Bootstrap</figcaption>
</figure>

Open the URL referenced in the CSS file and see the directory structure, notice files like `LICENSE` and `README.md`. Standard practice in framework development. You can confirm this by trying to access them on the victim machine, and as suspected, the files are still available.
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/jjupzFXRXLjURS58.png"><img src="https://blog.chadp.me/assets/images/luke/jjupzFXRXLjURS58.png"></a>
  <figcaption>The license file of Start Bootstrap is still available</figcaption>
</figure>

After identifying the framework being used, there is nothing too interesting that you can take advantage of. Try running `Gobuster` and specify the file type `php` to see if there is anything interesting:
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/MCRMenMY3Ki0ReFm.png"><img src="https://blog.chadp.me/assets/images/luke/MCRMenMY3Ki0ReFm.png"></a>
  <figcaption>Running gobuster on port 80</figcaption>
</figure>

From the `Gobuster` scan, there are a couple of interesting files and directories.
- `login.php`
- `member`
- `management`
   - Password protected directory, returning a 401
- `config.php`

Opening `login.php` results in a pretty basic login page, but no credentials have been found yet. The member directory is empty. As suspected, the management directory is password protected, so no access there. `config.php` has some pretty basic PHP that connects to a mysql database with the password for root.
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/qMwPY18mrvi0Qcl0.png"><img src="https://blog.chadp.me/assets/images/luke/qMwPY18mrvi0Qcl0.png"></a>
  <figcaption>The contents of `config.php`</figcaption>
</figure>

Trying these credentials in both the `login.php` form and the management page results in 403s, so keep these credentials for later use.

Try to access the server on port 3000. The first thing you see is that this is some form of JSON REST api as it returns a JSON object saying that must be authenticated:
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/wnhvPt2pzCGO7yLp.png"><img src="https://blog.chadp.me/assets/images/luke/wnhvPt2pzCGO7yLp.png"></a>
  <figcaption>The contents of the main page on port 3000</figcaption>
</figure>

A quick Google search of the returned message results in a post about [how to add JSON Web Tokens to a Node.js site](https://medium.com/dev-bits/a-guide-for-adding-jwt-token-based-authentication-to-your-single-page-nodejs-applications-c403f7cf04f4). The post indicates that you should login, get a Bearer token, and send it every time you send a request to the victim machine. Enumerate the victim machine using `Gobuster` to see if you can find any available endpoint targets. Ideally, one that accepts credentials.
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/kPVrIgm4QsMsB6Ap.png"><img src="https://blog.chadp.me/assets/images/luke/kPVrIgm4QsMsB6Ap.png"></a>
  <figcaption>Running `Gobuster` on port 3000</figcaption>
</figure>

The 2nd `Gobuster` scan finds 4 endpoints:
- `/login`
- `/users`
- `/Login`
- `/Users`

The last 2 are just used to allow non-case sensitive queries. Send the credentials you have to this login page and see what happens. Because I love Python so much, I whipped up a script to automate this:
```
import requests

creds = { "username": "admin", "password": "Zk6heYCyv6ZE9Xcg" }
resp = requests.post("http://10.10.10.137:3000/login", json=creds)
print(resp.json())
```
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/46VGjMMR6Nxn8uzA.png"><img src="https://blog.chadp.me/assets/images/luke/46VGjMMR6Nxn8uzA.png"></a>
  <figcaption>Successful auth</figcaption>
</figure>

Running the script should result in a successful authentication. Note that the credentials we used are from the file config.php we found earlier, but the user has been changed from root to admin. Modify the script to send the token received to the home page. Format the token as described in the earlier post as follows:
```
#...
token = resp.json()["token"]

headers = { "authorization": "Bearer " + token }
resp = requests.get("http://10.10.10.137:3000/", headers=headers)
print(resp.json())
```
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/4qIOVxaSUvB4J2Im.png"><img src="https://blog.chadp.me/assets/images/luke/4qIOVxaSUvB4J2Im.png"></a>
  <figcaption>Getting a welcome message after successfully sending the correct token format</figcaption>
</figure>

After successfully authenticating with the token, use it on the `/users` endpoint for further enumeration. Modify the Python script again:
```
#...

import pprint

pp = pprint.PrettyPrinter()
resp = requests.get("http://10.10.10.137:3000/users", headers=headers)
pp.pprint(resp.json())
```
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/znKdDs6kkbvynbGY.png"><img src="https://blog.chadp.me/assets/images/luke/znKdDs6kkbvynbGY.png"></a>
  <figcaption>Getting a list of users on the `/users` endpoint</figcaption>
</figure>

A list of users! In REST Apis, you can get more detailed information about a resource by accessing the resource id (`/resource/:id`). On the victim machine, the resource is users and the usernames or their ids would be the resource id. If you want to access more information about `admin`, access either `/users/admin` or `/users/1`. Try modifying the Python script to test this theory out:
```
#...

users = resp.json()

for user in users:
    resp = requests.get("http://10.10.10.137:3000/users/" + user["name"], headers=headers)
    print(resp.json())
```
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/YT5Pz4Jhxs7TwpM4.png"><img src="https://blog.chadp.me/assets/images/luke/YT5Pz4Jhxs7TwpM4.png"></a>
  <figcaption>Getting user info on the `/users/:username` endpoint</figcaption>
</figure>

The `/users/:username` endpoint spits out the password for each of the user. Profit! Try using these credentials on some of the login pages you found earlier. None of the passwords work on `/login.php`. `Derry` (the same user who wrote the letter to Chihiro) works on the `/management` page and spits out another directory listing.
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/lZ96d2pLYXPZeWSW.png"><img src="https://blog.chadp.me/assets/images/luke/lZ96d2pLYXPZeWSW.png"></a>
  <figcaption>Directory listing after logging in to `/management`</figcaption>
</figure>

Results of the 2nd directory listing:
- `config.json`
- `config.php`
- `login.php`

Take a quick look at `config.php` and `login.php`. These files are the same that were found in the root directory. `config.json` has a JSON config file for `Ajenti` which is an interesting find. `Ajenti` is the HTTP Control Panel that was found during the Nmap scan. Important items from this config are the username (root), it's password, and bind port 8000. So this clearly matches the `Ajenti HTTP Control Panel` found earlier:
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/amQHwwN3fPvYhMdU.png"><img src="https://blog.chadp.me/assets/images/luke/amQHwwN3fPvYhMdU.png"></a>
  <figcaption>Contents of the config.json file</figcaption>
</figure>

### Getting user.txt and root.txt flags

Take these credentials and try them on the `Ajenti HTTP Control Panel`. Successful login!
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/SVAtlRKTC05GKuzH.png"><img src="https://blog.chadp.me/assets/images/luke/SVAtlRKTC05GKuzH.png"></a>
  <figcaption>Main page of the Ajenti HTTP Control Panel after login</figcaption>
</figure>

On the `Ajenti HTTP Control Panel`, launch a terminal and find out that this victim machine is lacking a lot of IAM controls. Grab user and root flags without any escalation. Look Mom, I'm a hacker!
<figure>
    <a href="https://blog.chadp.me/assets/images/luke/SWnKvKYQzsl10hZE.png"><img src="https://blog.chadp.me/assets/images/luke/SWnKvKYQzsl10hZE.png"></a>
  <figcaption>User and Root Flags</figcaption>
</figure>
