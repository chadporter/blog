---
title: "SwagShop"
classes: wide
excerpt: "This post is a write-up for the Swagshop box on [hackthebox.eu](https://www.hackthebox.eu)"
date: 2019-09-28T01:00:00-05:00
tags:
  - hackthebox
  - swagshop
  - htb
  - writeups
  - Magento
  - webapp
  - web
  - exploit
  - sudo
---
This post is a write-up for the SwagShop box on hackthebox.eu
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/WknaI3ZfuWABY7q8.png"><img src="https://blog.chadp.me/assets/images/swagshop/WknaI3ZfuWABY7q8.png"></a>
  <figcaption>SwagShop Info</figcaption>
</figure>

### Enumeration

Start enumerating the ports on the victim machine by running `Nmap` and `Masscan`:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/yvJd8JoI4lQ1JDIi.png"><img src="https://blog.chadp.me/assets/images/swagshop/yvJd8JoI4lQ1JDIi.png"></a>
  <figcaption>Running nmap reveals two open ports</figcaption>
</figure>

Running nmap reveals the following information:
- Port 22
   - SSH Server
- Port 80
   - Seems to be running a web server.

Open a web browser to view the webapp:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/OPfsLM2ylB0UY3xz.png"><img src="https://blog.chadp.me/assets/images/swagshop/OPfsLM2ylB0UY3xz.png"></a>
  <figcaption>Magento Shopping Cart</figcaption>
</figure>

I had a quick look around the application and couldn’t come up with anything useful to gain access to the admin page (`http://10.10.10.140/index.php/admin`) which was found from a gobuster scan. A quick Google search results in a [Magento Shoplift SQLi](https://github.com/joren485/Magento-Shoplift-SQLI) exploit. This looks like Magento could possibly contain a SQL injection to create a new user and password to gain access to the admin section.

Git clone the exploit and run it:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/KSF5rGfezVCJPjfA.png"><img src="https://blog.chadp.me/assets/images/swagshop/KSF5rGfezVCJPjfA.png"></a>
  <figcaption>Running modified Magento eCommerce - Remote Code Execution exploit</figcaption>
</figure>

Login to the webapp with the credentials provided to access to the admin portal.
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/qYr6twzsN04e71xN.png"><img src="https://blog.chadp.me/assets/images/swagshop/qYr6twzsN04e71xN.png"></a>
  <figcaption>Magento Admin</figcaption>
</figure>

I had a good look around to see if I could find anything but reverted to looking for a public exploit for this. I eventually came across an article that mentions taking advantage of an upload function to get a reverse shell. https://blog.scrt.ch/2019/01/24/magento-rce-local-file-read-with-low-privilege-admin-rights/

### Gaining a shell
Go to `Catalog` > `Manage Products`. Continue to add a new product with an arbitrary name. Make sure you fill in all of the relevant information to make sure you can add your product to the shopping cart:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/PKDxABCHBHMDZqR4.png"><img src="https://blog.chadp.me/assets/images/swagshop/PKDxABCHBHMDZqR4.png"></a>
  <figcaption>New Product Settings</figcaption>
</figure>
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/acUx4HGxDyfXetwH.png"><img src="https://blog.chadp.me/assets/images/swagshop/acUx4HGxDyfXetwH.png"></a>
  <figcaption>New Product Settings</figcaption>
</figure>
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/jxpXrhdlDAAaMKGI.png"><img src="https://blog.chadp.me/assets/images/swagshop/jxpXrhdlDAAaMKGI.png"></a>
  <figcaption>New Product Settings</figcaption>
</figure>
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/4qCZ7tWqKMviSbwg.png"><img src="https://blog.chadp.me/assets/images/swagshop/4qCZ7tWqKMviSbwg.png"></a>
  <figcaption>New Product Settings</figcaption>
</figure>

After filling in all of the requirements, browse to the product on the front page. Grab a [PHP Reverse Shell](https://github.com/pentestmonkey/php-reverse-shell), modify the parameters, rename the file to `rev.phtml`, upload it on the shopping cart page, and click on add to cart:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/s7Q4k3dQlrzwpY5m.png"><img src="https://blog.chadp.me/assets/images/swagshop/s7Q4k3dQlrzwpY5m.png"></a>
  <figcaption>Magento Shopping Cart</figcaption>
</figure>

Great! Now where did our reverse shell end up? Looking at the article, the `.phtml` file is uploaded to `http://10.10.10.140/media/custom_options/quote/quote/firstLetterOfYourOriginalFileName/secondLetterOfYourOriginalFileName`.

Fire up a nc listener, find the upload, and click on it:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/qYNUeyGSrzjLAMBv.png"><img src="https://blog.chadp.me/assets/images/swagshop/qYNUeyGSrzjLAMBv.png"></a>
  <figcaption>Upload link location</figcaption>
</figure>

Grab the user flag:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/hKuKBHCDxSDU6dUj.png"><img src="https://blog.chadp.me/assets/images/swagshop/hKuKBHCDxSDU6dUj.png"></a>
  <figcaption>User flag</figcaption>
</figure>

### Root flag

With a user shell on the victim machine, go through the [Rebootuser Local Linux Enumeration & Privilege Escalation Cheatsheet](https://www.rebootuser.com/?p=1623), and notice an interesting response to `sudo -l`. The user has sudo permissions to run `vi` only on file in `/var/www/html/*`. See what files you can edit to possibly escalate privileges:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/DF2gw5R5eT7SLsDU.png"><img src="https://blog.chadp.me/assets/images/swagshop/DF2gw5R5eT7SLsDU.png"></a>
  <figcaption>Directory listing of `/var/www/html/`</figcaption>
</figure>

You can use any file in `/var/www/html/`, as you are simply using it for a shell escape. Run `sudo /usr/bin/vi /var/www/html/cron.sh`, and add `set shell=/bin/sh` into the script:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/GWJTZ5Kaqt4tcqhO.png"><img src="https://blog.chadp.me/assets/images/swagshop/GWJTZ5Kaqt4tcqhO.png"></a>
  <figcaption>Directory listing of `/var/www/html/`</figcaption>
</figure>

Exit insert mode, run `:shell` to escape into a root shell, and grab the root flag:
<figure>
    <a href="https://blog.chadp.me/assets/images/swagshop/0vW3r6FmpkKdOhYk.png"><img src="https://blog.chadp.me/assets/images/swagshop/0vW3r6FmpkKdOhYk.png"></a>
  <figcaption>Root flag</figcaption>
</figure>
