---
title: "GitHubbub! GitHub Does Not Value Free Software"
permalink: /about/githubbub/
---

[Free software](https://www.gnu.org/philosophy/free-sw.html) guarantees your freedom to study, modify, and share the software that you use. We value these freedoms on the desktop, so why should we compromise when websites serve proprietary JavaScript [just because it creates the illusion of remote execution](https://www.gnu.org/software/easejs/whyfreejs.html)? When you visit a website that serves JavaScript to the client, your web browser is automatically [downloading and executing](https://www.gnu.org/philosophy/javascript-trap.html) (often without your permission) ephemeral, unsigned, untrusted software. If that JavaScript is not [freely licensed](https://www.gnu.org/software/librejs/free-your-javascript.html), then the software running in your web browser is proprietary.

**When you visit `github.com`, you download over 200kB of obfuscated code, much of which is proprietary.** This code provides many website features that are fairly essential, and *do not work with JavaScript disabled:*
* Change repository names or descriptions;
* Delete repositories;
* Add an SSH key to your account;
* Fork repositories;
* Create pull requests;
* Enable and disable project features;
* Use the wiki and issue trackers;
* View graphs of statistics;
* And others.

That is—GitHub forces you to run proprietary software in order to use much of their website. This is a bit startling for a host that owes its very existence to the success and development of free software.